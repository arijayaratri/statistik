<script language='JavaScript'>
	var ajaxRequest;
	function getAjax() //fungsi untuk mengecek AJAX pada browser
	{
		try
		{
			ajaxRequest = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) 
			{
				try
				{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser broke!");
					return false;
				}
			}
		}
	}

	function autoComplete_b(idInput_b) //fungsi menangkap input_b search dan menampilkan hasil search berdasarkan											  
	 //idInput_b text yang dijadikan indikator search
	{
		getAjax();
		input_b = document.getElementById('inputan_b'+idInput_b).value;

		if (input_b == "")
		{
			document.getElementById("hasil_b" + idInput_b).innerHTML = "";
		}
		else
		{
			ajaxRequest.open("GET","ajax/php/search_iku_b.php?input_b="+input_b+"&idInput_b="+idInput_b+"&id_induk="+<?php echo $id_induk?>);
			ajaxRequest.onreadystatechange = function()
			{
				document.getElementById("hasil_b" + idInput_b).innerHTML = ajaxRequest.responseText;
			}
			ajaxRequest.send(null);
		}
	}

	function autoInsert_b(nama, idInput_b) //fungsi mengisi input text dengan hasil_b pencarian yang dipilih
	{
		document.getElementById("inputan_b"+idInput_b).value = nama;
		document.getElementById("hasil_b"+idInput_b).innerHTML = "";
	}
	</script>