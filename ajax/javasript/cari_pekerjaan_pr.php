<script language='JavaScript'>
	var ajaxRequest;
	function getAjax() //fungsi untuk mengecek AJAX pada browser
	{
		try
		{
			ajaxRequest = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) 
			{
				try
				{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser broke!");
					return false;
				}
			}
		}
	}

	function autoComplete_pr(idInput_pr) //fungsi menangkap input_pr search dan menampilkan hasil search berdasarkan											  
	 //idInput_pr text yang dijadikan indikator search
	{ 
		getAjax();
		input_pr = document.getElementById('inputan_pr'+idInput_pr).value;

		if (input_pr == "")
		{
			document.getElementById("hasil_pr" + idInput_pr).innerHTML = "";
		}
		else
		{
			ajaxRequest.open("GET","ajax/php/search_pekerjaan_pr.php?input_pr="+input_pr+"&idInput_pr="+idInput_pr);
			ajaxRequest.onreadystatechange = function()
			{
				document.getElementById("hasil_pr" + idInput_pr).innerHTML = ajaxRequest.responseText;
			}
			ajaxRequest.send(null);
		}
	}

	function autoInsert_pr(nama, idInput_pr) //fungsi mengisi input text dengan hasil_pr pencarian yang dipilih
	{
		document.getElementById("inputan_pr"+idInput_pr).value = nama;
		document.getElementById("hasil_pr"+idInput_pr).innerHTML = "";
	}
</script>