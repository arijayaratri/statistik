<script language='JavaScript'>
	var ajaxRequest;
	function getAjax() //fungsi untuk mengecek AJAX pada browser
	{
		try
		{
			ajaxRequest = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) 
			{
				try
				{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser broke!");
					return false;
				}
			}
		}
	}

	function autoComplete_opd(idInput_opd) //fungsi menangkap input_opd search dan menampilkan hasil search berdasarkan											  
	 //idInput_opd text yang dijadikan indikator search
	{
		getAjax();
		input_opd = document.getElementById('inputan_opd'+idInput_opd).value;

		if (input_opd == "")
		{
			document.getElementById("hasil_opd" + idInput_opd).innerHTML = "";
		}
		else
		{
			ajaxRequest.open("GET","ajax/php/search_opd_opd.php?input_opd="+input_opd+"&idInput_opd="+idInput_opd);
			ajaxRequest.onreadystatechange = function()
			{
				document.getElementById("hasil_opd" + idInput_opd).innerHTML = ajaxRequest.responseText;
			}
			ajaxRequest.send(null);
		}
	}

	function autoInsert_opd(nama, idInput_opd) //fungsi mengisi input text dengan hasil_opd pencarian yang dipilih
	{
		document.getElementById("inputan_opd"+idInput_opd).value = nama;
		document.getElementById("hasil_opd"+idInput_opd).innerHTML = "";
	}
</script>	
		
			