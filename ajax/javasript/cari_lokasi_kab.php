<script language='JavaScript'>
	var ajaxRequest;
	function getAjax() //fungsi untuk mengecek AJAX pada browser
	{
		try
		{
			ajaxRequest = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) 
			{
				try
				{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser broke!");
					return false;
				}
			}
		}
	}

	function autoComplete_kab(idInput_kab) //fungsi menangkap input_kab search dan menampilkan hasil search berdasarkan											  
	 //idInput_kab text yang dijadikan indikator search
	{
		getAjax();
		input_kab = document.getElementById('inputan_kab'+idInput_kab).value;

		if (input_kab == "")
		{
			document.getElementById("hasil_kab" + idInput_kab).innerHTML = "";
		}
		else
		{
			ajaxRequest.open("GET","ajax/php/search_kabupaten_kab.php?input_kab="+input_kab+"&idInput_kab="+idInput_kab);
			ajaxRequest.onreadystatechange = function()
			{
				document.getElementById("hasil_kab" + idInput_kab).innerHTML = ajaxRequest.responseText;
			}
			ajaxRequest.send(null);
		}
	}

	function autoInsert_kab(nama, idInput_kab) //fungsi mengisi input text dengan hasil_kab pencarian yang dipilih
	{
		document.getElementById("inputan_kab"+idInput_kab).value = nama;
		document.getElementById("hasil_kab"+idInput_kab).innerHTML = "";
	}
</script>	
		
			