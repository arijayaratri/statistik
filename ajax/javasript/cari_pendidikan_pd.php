<script language='JavaScript'>
	var ajaxRequest;
	function getAjax() //fungsi untuk mengecek AJAX pada browser
	{
		try
		{
			ajaxRequest = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) 
			{
				try
				{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser broke!");
					return false;
				}
			}
		}
	}

	function autoComplete_pd(idInput_pd) //fungsi menangkap input_pd search dan menampilkan hasil search berdasarkan											  
	 //idInput_pd text yang dijadikan indikator search
	{
		getAjax();
		input_pd = document.getElementById('inputan_pd'+idInput_pd).value;

		if (input_pd == "")
		{
			document.getElementById("hasil_pd" + idInput_pd).innerHTML = "";
		}
		else
		{
			ajaxRequest.open("GET","ajax/php/search_pendidikan_pd.php?input_pd="+input_pd+"&idInput_pd="+idInput_pd);
			ajaxRequest.onreadystatechange = function()
			{
				document.getElementById("hasil_pd" + idInput_pd).innerHTML = ajaxRequest.responseText;
			}
			ajaxRequest.send(null);
		}
	}

	function autoInsert_pd(nama, idInput_pd) //fungsi mengisi input text dengan hasil_pd pencarian yang dipilih
	{
		document.getElementById("inputan_pd"+idInput_pd).value = nama;
		document.getElementById("hasil_pd"+idInput_pd).innerHTML = "";
	}
</script>	
		
			