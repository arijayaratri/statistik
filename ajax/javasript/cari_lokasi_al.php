<script language='JavaScript'>
	var ajaxRequest;
	function getAjax() //fungsi untuk mengecek AJAX pada browser
	{
		try
		{
			ajaxRequest = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e) 
			{
				try
				{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser broke!");
					return false;
				}
			}
		}
	}

	function autoComplete_al(idInput_al) //fungsi menangkap input_al search dan menampilkan hasil search berdasarkan											  
	 //idInput_al text yang dijadikan indikator search
	{
		getAjax();
		input_al = document.getElementById('inputan_al'+idInput_al).value;

		if (input_al == "")
		{
			document.getElementById("hasil_al" + idInput_al).innerHTML = "";
		}
		else
		{
			ajaxRequest.open("GET","ajax/php/search_lokasi_al.php?input_al="+input_al+"&idInput_al="+idInput_al);
			ajaxRequest.onreadystatechange = function()
			{
				document.getElementById("hasil_al" + idInput_al).innerHTML = ajaxRequest.responseText;
			}
			ajaxRequest.send(null);
		}
	}

	function autoInsert_al(nama, idInput_al) //fungsi mengisi input text dengan hasil_al pencarian yang dipilih
	{
		document.getElementById("inputan_al"+idInput_al).value = nama;
		document.getElementById("hasil_al"+idInput_al).innerHTML = "";
	}
</script>	
		
			