<!DOCTYPE HTML>
<html>
<head>  
<meta charset="UTF-8">
<?php
$datax		= "";
$jdl		= explode(",",$judulx);
$kxy		= explode(",",$kolomx);	
$jml_kx		= count($kxy);
for($y=0;$y< $jml_kx;$y++)
{
$knya		= $kxy[$y];
$jdlnya		= $jdl[$y];
for($x = $jml_tahun-1;$x >= 0;$x-- )
{
$thnx		= $tahun - $x;

$isinya		= "";
$sql_dt		= "select sum(k) as k,nama,id_master from 
			(
			select 0 as k,nama,id as id_master from m_data where id_kelompok='$id_kelompok'
			union all
			select ta_data.{$knya} as k,m_data.nama,m_data.id as id_master from m_data,ta_data
			where m_data.id=ta_data.id_master and m_data.id_kelompok='$id_kelompok' and ta_data.tahun='$thnx'
			)as hasil group by id_master";
//echo "$y =$sql_dt<br>";
$query_dt	= mysqli_query($konek,$sql_dt);
while($hasil_dt	= mysqli_fetch_array($query_dt))
	{
	$k		= $hasil_dt['k'];	
	$nama	= $hasil_dt['nama'];
	$isi	= "{label: '$nama', y: $k}";
	$isinya	= "$isinya,$isi";
	}
	//echo "$y =$sql_dt<br>$isinya<br>";
$isinya = substr($isinya,1);
//echo "$isinya<br>";
if($x >= 1 and $y > 1 )	{$pos 			= "axisYType: 'secondary',";} 	else {$pos 			= "";}
if($k_grafik > 0)		{$judul_bawah	= "$thnx";} 					else {$judul_bawah	= "$thnx $jdlnya";}
while(strlen($judul_bawah) < 200)
{
$judul_bawah	= "$judul_bawah$ksg";	
}
$judul_bawah	= "$judul_bawah .";
$datanya	="	{
		type: 'column',
		legendText: '$judul_bawah',
		showInLegend: true,
		{$pos} 
		dataPoints:[$isinya]
	}";
//echo "$datanya<br>";
$datax	= "$datax,$datanya";
}
$isinya		= "";
$sql_dt		= "select 0 as k,nama,id as id_master from m_data where id_kelompok='$id_kelompok'";
//echo "$y =$sql_dt<br>";
$query_dt	= mysqli_query($konek,$sql_dt);
while($hasil_dt	= mysqli_fetch_array($query_dt))
	{
	$k		= $hasil_dt['k'];	
	$nama	= $hasil_dt['nama'];
	$isi	= "{label: '$nama', y: $k}";
	$isinya	= "$isinya,$isi";
	}
	//echo "$y =$sql_dt<br>$isinya<br>";
$isinya = substr($isinya,1);
//echo "$isinya<br>";
if($x >= 1 and $y > 0 ){$pos = "axisYType: 'secondary',";} else {$pos = "";}
$judul_bawah	= ".";
while(strlen($judul_bawah) < 200)
{
$judul_bawah	= "$judul_bawah$ksg";	
}
$judul_bawah	= "$judul_bawah .";
$datanya	="	{
		type: 'column',
		legendText: '$judul_bawah',
		showInLegend: true,
		{$pos} 
		dataPoints:[$isinya]
	}";
//echo "$datanya<br>";
$datax	= "$datax,$datanya";
}
$datax	= substr($datax,1);
//echo $datax;
?>
<script>
window.onload = function () {
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: "<?php echo "$judul_atas";?>",
		fontSize: 30,
		fontFamily: "calibri"
	},	
	axisY: {
		titleFontColor: "black",
		lineColor: "black",
		labelFontColor: "black",
		tickColor: "black"
	},	
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [
	<?php echo $datax?>
	]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

}
</script>
</head>
<body>
<div id="chartContainer" style="height: 570px; max-width: 1400px; margin: 0px auto;"></div>
<script src="canvasjs/canvasjs.min.js"></script>
</body>
</html>