<?php
$datax	= "";
for($x = 0;$x <= $jml_tahun;$x++ )
{
$thnx	= $tahun - $x;
$isinya		= "";			
$sql_dt		= "select sum(k1) as k1,nama,id_master from 
			(
			select 0 as k1,nama,id as id_master from m_data where id_kelompok='$id_kelompok'
			union all
			select ta_data.k1,m_data.nama,m_data.id as id_master from m_data,ta_data
			where m_data.id=ta_data.id_master and m_data.id_kelompok='$id_kelompok' and ta_data.tahun='$thnx'
			)as hasil group by id_master";
$query_dt	= mysqli_query($konek,$sql_dt);
while($hasil_dt	= mysqli_fetch_array($query_dt))
	{
	$k1		= $hasil_dt['k1'];	
	$nama	= $hasil_dt['nama'];
	$isi	= "{label: '$nama', y: $k1}";
	$isinya	= "$isinya,$isi";
	}
$isinya = substr($isinya,1);
$datanya	="	{
		type: 'column',
		name: 'Proven Oil Reserves (bn)',
		legendText: '$thnx',
		showInLegend: true, 
		dataPoints:[
			$isinya
		]
	}";
$datax	= "$datax,$datanya";
}
$datax	= substr($datax,1);
//echo $datax;
?>
<script>
window.onload = function () 
{
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title:{
		text: "<?php echo "$nama_kelompok";?>"
	},	
	axisY: {
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisY2: {
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E"
	},	
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [
	<?php echo $datax?>
	]
});
chart.render();

function toggleDataSeries(e) 
{
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}
}
</script>
<div id="chartContainer" style="height:370px;max-width:920px;margin:0px auto;"></div>
<script src="canvasjs/canvasjs.min.js"></script>
