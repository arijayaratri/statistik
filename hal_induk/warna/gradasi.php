<?

$warna1="white";
$warna2="#3366FF";
$warna3="black";
$warna4="#DDE8DE";
$warna5="#3366FF";

$gradasi1 ="
/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(bottom, $warna1 0%, $warna2 100%);

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(bottom, $warna1 0%, $warna2 100%);

/* Opera */ 
background-image: -o-linear-gradient(bottom, $warna1 0%, $warna2 100%);

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, $warna1), color-stop(1, $warna2));

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(bottom, $warna1 0%, $warna2 100%);

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to top, $warna1 0%, $warna2 100%);";

$gradasi2 ="
/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(bottom, $warna1 0%, $warna4 100%);

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(bottom, $warna1 0%, $warna4 100%);

/* Opera */ 
background-image: -o-linear-gradient(bottom, $warna1 0%, $warna4 100%);

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, $warna1), color-stop(1, $warna4));

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(bottom, $warna1 0%, $warna4 100%);

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to top, $warna1 0%, $warna4 100%);";

$gradasi3 ="
/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(bottom, $warna1 0%, $warna5 100%);

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(bottom, $warna1 0%, $warna5 100%);

/* Opera */ 
background-image: -o-linear-gradient(bottom, $warna1 0%, $warna5 100%);

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, $warna1), color-stop(1, $warna5));

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(bottom, $warna1 0%, $warna5 100%);

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to top, $warna1 0%, $warna5 100%);";

?>