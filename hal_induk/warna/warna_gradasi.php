<?
$warna1="white";
$warna2="green";
?>

<style type="text/css">
div.terang-atas
{
width:95%;
border-style:solid;
border-color:rgb(225, 225, 225);
-moz-border-radius: 0px 0px 0px 0px;
-webkit-border-radius: 0px 0px 0px 0px;
padding: 5px;
background: -moz-linear-gradient(center top, <?echo $warna1;?> 0%, <?echo $warna2;?> 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0,<?echo $warna1;?>),color-stop(1,<?echo $warna2;?>));
}
</style>
<style type="text/css">
div.terang-atas-tanpa
{
width:95%;
padding: 5px;
background: -moz-linear-gradient(center top, <?echo $warna1;?> 0%, <?echo $warna2;?> 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0,<?echo $warna1;?>),color-stop(1,<?echo $warna2;?>));
}
</style>
<style type="text/css">
div.terang-atas-tanpa-akhir
{
width:80%;
padding: 5px;
background: -moz-linear-gradient(center top, <?echo $warna1;?> 0%, <?echo $warna2;?> 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0,<?echo $warna1;?>),color-stop(1,<?echo $warna2;?>));
}
</style>
<style type="text/css">
div.terang-atas-tanpa2
{
width:95%;
padding: 15px;
background: -moz-linear-gradient(center top, <?echo $warna1;?> 0%, <?echo $warna2;?> 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0,<?echo $warna1;?>),color-stop(1,<?echo $warna2;?>));
}
</style>
<style type="text/css">
div.terang-atas-tanpa2-akhir
{
width:60%;
padding: 15px;
background: -moz-linear-gradient(center top, <?echo $warna1;?> 0%, <?echo $warna2;?> 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0,<?echo $warna1;?>),color-stop(1,<?echo $warna2;?>));
}
</style>
<style type="text/css">
div.pipa
{
width:95%;
border-style:solid;
border-color:rgb(225, 225, 225);
-moz-border-radius: 0px 0px 0px 0px;
-webkit-border-radius: 0px 0px 0px 0px;
padding: 5px;
margin-left: 0px;
background: -moz-linear-gradient(center top, <?echo $warna2;?> 0%,<?echo $warna1;?> 50%,<?echo $warna2;?> 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0, <?echo $warna2;?>),color-stop(0.5, <?echo $warna1;?>),color-stop(1, <?echo $warna2;?>));
}
</style>

